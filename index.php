<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>SGInteractive Test 2</title>

    </head>
    <body>
        <p>
            Problem 2: Implement a basic spin results end point</p>
        <ul>
            <li>
                Description
                <ul>
                    <li>
                        Generate a JSON response with the following data:
                        <ul>
                            <li>
                                Player ID</li>
                            <li>
                                Name</li>
                            <li>
                                Credits</li>
                            <li>
                                Lifetime Spins</li>
                            <li>
                                Lifetime Average Return</li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
        <p>
            <strong><span style="color:#ff0000;">NOTE</span></strong>: Salt was created randomly this way:</p>
        <div>
            $salt = uniqid(mt_rand(), true);</div>
        <div>
            $options = [</div>
        <div>
            &nbsp; &nbsp; &#39;cost&#39; =&gt; 11,</div>
        <div>
            &nbsp; &nbsp; &#39;salt&#39; =&gt; $salt,</div>
        <div>
            ];</div>
        <div>
            &nbsp;</div>
        <div>
            And hash was created using salt as password, and salt:</div>
        <div>
            &nbsp;</div>
        <div>
            $hash = password_hash($salt, PASSWORD_BCRYPT, $options);</div>
        <div>
            &nbsp;</div>
        <h3>
            Players with hash:</h3>
        <ul>
            <li>
                Dominic Ricketson - &nbsp;$2y$11$90947385556dda60bb9e1u8/y4ZU/Zq0Z8PA7pMYk56Ea29vHLyey&nbsp;</li>
            <li>
                Shirly Basinger - $2y$11$175218701756dda60c4b6elrMU4PPCNp/HY2Mk9rdekFCTjNWIxCS&nbsp;</li>
            <li>
                Federico Humbert - $2y$11$13827183056dda60ca897uQWX0SQ7u83NLIR5KiwqAEoMu4C2TYZe&nbsp;</li>
            <li>
                Jenell Silsby - $2y$11$179542704356dda60d11bO19G5KOIjzt2WMeq7ZjVWVTyIWm1XlSi&nbsp;</li>
            <li>
                Wilfred Neiman - $2y$11$175661079956dda60d6f0OomOuj4OPA6W4F8OAjfVuGf/ppYlG1Ce&nbsp;</li>
            <li>
                Emmy Rathbun - $2y$11$91798197056dda60dccd2O69isFu2m7XBCelhXnWrXAlL/p1wnEJy&nbsp;</li>
            <li>
                Juana Orlandi - $2y$11$172177521856dda60e360uXZ5LyrL9murReVLQukNOhsW5icLr1y.&nbsp;</li>
            <li>
                Reanna Kitch - $2y$11$213806430856dda60e937uFzj/NyL0KWozKjJdMsR9G.yQHAuPXAy&nbsp;</li>
            <li>
                Towanda Robinett - $2y$11$54274408456dda60ef0f5eSneTkylM/8/SKQSpmHJJWKjtJX3hjna&nbsp;</li>
            <li>
                Tena Highland - $2y$11$13816982956dda60f5badu1gB5jQeWKskuHg/2esIgm8iLZ./Y6/a&nbsp;</li>
        </ul>
        <h3>
            Tests</h3>
        <ul>
            <li><a href="/SGInteractive-problem2/service.php?hash=$2y$11$90947385556dda60bb9e1u8/y4ZU/Zq0Z8PA7pMYk56Ea29vHLyey&won=10&bet=15&id=1">All parameters valid</a></li>
            <li><a href="/SGInteractive-problem2/service.php?hash=$2y$11$90947385556dda60bb9e1u8/y4ZU/Zq0Z8PA7pMYk56Ea29vHLyeybet=15&id=1">Missing query string</a></li>
            <li><a href="/SGInteractive-problem2/service.php?hash=$2y$11$90947385556dda60bb9e1u8/y4ZU/Zq0Z8PA7pMYk56Ea29vHLyey&won=1k0&bet=15&id=1">Query string with wrong value type</a></li>
            <li><a href="/SGInteractive-problem2/service.php?hash=$2y$11$9094kjhhj556dda60bb9e1u8/y4ZU/Zq0Z8PA7pMYk56Ea29vHLyey&won=10&bet=15&id=1">Bad hash</a></li>
            <li><a href="/SGInteractive-problem2/service.php?hash=$2y$11$90947385556dda60bb9e1u8/y4ZU/Zq0Z8PA7pMYk56Ea29vHLyey&won=10&bet=15&id=1j">Bad ID</a></li>
            <li><a href="/SGInteractive-problem2/service.php?hash=$2y$11$90947385556dda60bb9e1u8/y4ZU/Zq0Z8PA7pMYk56Ea29vHLyey&won=10&bet=15&id=111">ID does not exist</a></li>
            <li><a href="/SGInteractive-problem2/service.php">No parameters</a></li>
        </ul>
        <p>
            &nbsp;</p>
    </body>
</html>
