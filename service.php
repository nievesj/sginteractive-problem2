<?php

/*
 * Author: José M Nieves
 * Date: 3/7/2016
 * Purpose: SGInteractive Programming test
 * Description: Generate a JSON response with the following data:
    *Player ID
    *Name
    *Credits
    *Lifetime Spins
    *Lifetime Average Return
 */

//RedBeanPHP ORM
require 'rb.php';

//set header to json
header('Content-Type: application/json');

//request querystring
parse_str($_SERVER['QUERY_STRING'], $output);

//used to determine if there was a validation error
$isError = false;

//if $output not empty continue
if (!empty($output)) {

    //check required array keys
    if (isset($output['hash']) && isset($output['won']) && isset($output['bet']) && isset($output['id'])) {

        //check if won and bet are numeric values
        if (is_numeric($output['won']) && is_numeric($output['bet']) && is_numeric($output['id'])) {
            //Everything is ok up to this point... get player info, calculate and return json response
            GetPlayer($output);
        } else {
            $isError = true;
            $error = array('error' => "Parameters won, bet or ID are the wrong type. Please make sure they are a number.");
        }
    } else {
        $isError = true;
        $error = array('error' => "Missing required querystrrings");
    }
} else {
    //send a json response of the error
    $isError = true;
    $error = array('error' => "QueryString is empty");
}

//there's an error? Return it in json format
if ($isError) {
    echo json_encode($error);
}

function GetPlayer($arr) {
    //connect to database
    R::setup('mysql:host=mysql.josemnieves.net;dbname=sginteractive', 'sginteractive', 'nievesj@5673');

    //place array values into variables, for readability
    $hash = $arr['hash'];
    $coinswon = $arr['won'];
    $coinsbet = $arr['bet'];
    $playerid = $arr['id'];

    //get player
    $player = R::findOne('player', "id = $playerid");

    if (!empty($player)) {
        //validate hash
        if (ValidateHash($hash, $player->salt_value)) {
            //increment spin
            $player->lifetime_spins = $player->lifetime_spins + 1;

            //substract bet
            $player->credits = $player->credits - $coinsbet;

            //add wins
            $player->credits = $player->credits - $coinswon;

            //update record
            R::store($player);

            //calculate average spin result
            $asr = $player->credits / $player->lifetime_spins;

            //build json array
            $result = array('Player ID' => $player->id, 'Name' => $player->name, 'Credits' => $player->credits, 'Lifetime Spins' => $player->lifetime_spins, 'Lifetime Average Return' => $asr);

            //return json
            echo json_encode($result);
        } else {
            $error = array('error' => "Cannot validate hash");
            echo json_encode($error);
        }
    } else {
        $error = array('error' => "Cannot find Player ID $playerid");
        echo json_encode($error);
    }

    R::close();
}

function ValidateHash($hash, $seed) {

    //for simplicity the seed is the password as we are not getting any other parameters to generate a hash with
    return password_verify($seed, $hash);
}
